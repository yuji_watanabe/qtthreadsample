﻿#include <QCoreApplication>
#include <QDebug>
#include <QThread>
#include <QTimer>
#include <QWaitCondition>

class WorkerProcess: public QObject
{
    Q_OBJECT
signals:
    void receive();

public slots:
    void process() {
        for (int i = 0; i < 10; ++i) {
            qDebug() << "process: " << i;
        }
        emit receive();
    }
};

class WorkerReceiver: public QObject
{
    Q_OBJECT

    QWaitCondition *m_wait;
public:
    WorkerReceiver(QWaitCondition *condition): m_wait(condition){}

public slots:
    void received() {
        qDebug() << "data received";
        m_wait->wakeOne();
    }
};

class Worker : public QObject
{
    Q_OBJECT

    QMutex m_mutex;
    QWaitCondition m_wait;
public:
    QWaitCondition *condition() {
        return &m_wait;
    }

signals:
    void startProcess();

public slots:
    void execute() {
        QMutexLocker locker(&m_mutex);
        qDebug() << "execute doSomething get called from ?: " << QThread::currentThreadId();
        doSomething();
        if (!m_wait.wait(&m_mutex, 100)) {
            qDebug() << "timeout";
            return;
        }
        qDebug() << "doSomething done";
    }
    void received() {
        qDebug() << "data received";
        m_wait.wakeOne();
    }

    void onTimeout()
    {
        qDebug() << "worker::onTimeout get called from ?: " << QThread::currentThreadId();
    }

private:
    void doSomething() {
        emit startProcess();
    }
};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    qDebug() << "From main thread: " << QThread::currentThreadId();

    QThread thread;
    QTimer timer;
    WorkerProcess workerProcess;
    Worker worker;
    WorkerReceiver receiver(worker.condition());

    QObject::connect(&workerProcess, &WorkerProcess::receive, &receiver, &WorkerReceiver::received);
    QObject::connect(&worker, &Worker::startProcess, &workerProcess, &WorkerProcess::process);
    QObject::connect(&timer, &QTimer::timeout, &worker, &Worker::execute);
    //timer.singleShot(0, &worker, &Worker::execute);

    timer.start(1);

    worker.moveToThread(&thread);
    thread.start();

    return a.exec();
}

#include "main.moc"
